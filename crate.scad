// CSG.scad - Basic example of CSG usage

emt_length=120;
emt_takeup=6;
emt_arc=9.5;

base_height = 40;
base_length = 35;

arm_height = 24;
handle_length=arm_height-2*emt_takeup+2*emt_arc;

arm_length=(emt_length-handle_length)/2+emt_takeup;

// the base
translate([base_length/2,0,0])
color("red")
square([base_height,base_length],center=true);
echo(arm_length);


// the arm
color("green")
translate([-(arm_length-base_length),-arm_height/2,0]) {
square([arm_length,arm_height]);
}



/*
translate([-24,0,0]) {
    union() {
        cube(15, center=true);
        sphere(10);
    }
}

intersection() {
    cube(15, center=true);
    sphere(10);
}

translate([24,0,0]) {
    difference() {
        cube(15, center=true);
        sphere(10);
    }
}
*/

echo(version=version());
// Written by Marius Kintel <marius@kintel.net>
//
// To the extent possible under law, the author(s) have dedicated all
// copyright and related and neighboring rights to this software to the
// public domain worldwide. This software is distributed without any
// warranty.
//
// You should have received a copy of the CC0 Public Domain
// Dedication along with this software.
// If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
